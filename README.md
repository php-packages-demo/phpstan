# phpstan

[**phpstan/phpstan**](https://packagist.org/packages/phpstan/phpstan)
[![PHPPackages Rank](https://pages-proxy.gitlab.io/phppackages.org/phpstan/phpstan/rank.svg)](http://phppackages.org/s/phpstan)
[![PHPPackages Referenced By](https://pages-proxy.gitlab.io/phppackages.org/phpstan/phpstan/referenced-by.svg)](http://phppackages.org/s/phpstan)
PHP Static Analysis Tool

# Unofficial documentation
* [*State of Generics and Collections*
  ](https://thephp.foundation/blog/2024/08/19/state-of-generics-and-collections/)
  2024-08 Arnaud Le Blanc, Derick Rethans, Larry Garfield
* [*Generics By Examples*
  ](https://phpstan.org/blog/generics-by-examples)
  2022-03 Ondřej Mirtes (PHPStan)
* [*Creating custom PHPStan rules for your Symfony project*
  ](https://www.strangebuzz.com/en/blog/creating-custom-phpstan-rules-for-your-symfony-project)
  2021-10 C0il
* [*How to ensure PHP code quality? Static analysis using PHPStan with Symfony*
  ](https://medium.com/accesto/how-to-ensure-php-code-quality-static-analysis-using-phpstan-with-symfony-accesto-blog-39fb57545e44)
  2021-07 Michał Romańczuk
* [*How to configure PHPStan for Symfony applications*
  ](https://blog.martinhujer.cz/how-to-configure-phpstan-for-symfony-applications/)
  2019-10 Martin Hujer

# Build onPHPStan
* [rector/rector](https://packagist.org/packages/rector/rector)
